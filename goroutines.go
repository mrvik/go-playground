package main

import (
    "fmt"
    "io/ioutil"
    "os"
)
type callback func(chan string)
type goroutine func(string,chan string)string

func check(e error) {
    if e != nil {
        panic(e)
    }
}

func main() {
    var paths []string
    if len(os.Args) > 1 {
        paths = os.Args[1:]
    } else {
        paths[0] = "goroutines.go"
    }
    completionChannel := make(chan int,2);
    //listFiles(paths);
    iterate(paths, ls, iterateDirs, completionChannel)
    if(<-completionChannel==1){
        fmt.Println("Finished OK");
    }else{
        fmt.Println("Child goroutine returned nok status")
    }
}
func iterate(paths []string, routine goroutine, cb callback, completion chan int) {
    msgpool := make([]chan string, len(paths))
    for k, path := range paths {
        channel := make(chan string)
        msgpool[k] = channel
        go routine(path, channel)
    }
    for _, ch := range msgpool {
        cb(ch)
    }
    completion <- 1;
}

//Specific iterator callbacks
func iterateFiles(channel chan string){
    path := <-channel
    fmt.Println("Printing contents of " + path)
    contents := <- channel
    fmt.Print(string(contents))
}
func iterateDirs(channel chan string){
    path:= <-channel
    fmt.Println("Printing tree of "+path)
    tree:= <- channel
    fmt.Print(string(tree))
}

//Single argument functions
func readFile(path string, channel chan string) string {
    channel <- string(path)
    dat, err := ioutil.ReadFile(path)
    check(err)
    channel <- string(dat)
    return string(dat)
}
func ls(path string,channel chan string) string {
    channel <- string(path)
    dat,err := ioutil.ReadDir(path);
    check(err)
    var strdat string
    for _,info := range dat {
        strdat=strdat+fmt.Sprintf("Name: %s \nMode: %s \n",info.Name(),info.Mode());
    }
    channel <- strdat
    return strdat
}
